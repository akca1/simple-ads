<?php
/**
 * Created by PhpStorm.
 * User: Mehmet
 * Date: 03.12.16
 * Time: 12:50
 */

include_once ('config.inc.php');

function verify($uname, $upass, $umail,$db)
{
//    echo "regex:".$email_valid_regex;
    if (!preg_match(email_valid_regex, $upass)) {
        echo '<div id="main">' . "Error:Passwortrichtlinie beachten" . '</div>';
        return false;
    }
    if ($umail == "I'm here only for verifying password") {
        return true;
    }

    if (!checkEmailAndDomain($umail)) {
        echo '<div id="main">' . "Error:Email stimmt nicht. Bitte beachten!" . '</div>';
        return false;
    }

    $stmt = $db->prepare("Select ID from User where user = :username");
    $stmt->bindParam(':username', $uname);

    if ($stmt->execute()) {
        $count = $stmt->rowCount();
        //var_dump($count);
        if ($count > 0) {
            echo 'Bitte einen anderen Usernamen aussuchen!' . $uname;
            return false;
        }

    }
    return true;
}
?>