<?php
session_start();
require_once 'htmlpurifier/library/HTMLPurifier.auto.php';
include_once ('config.inc.php');
require_once "verify.php";



try {
    $db = new PDO(mysql,dbuser,dbpass);


    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);

    if(isset($_SESSION['user_session']) and isset($_REQUEST['mode']))
    {
        if($_REQUEST['mode']=='ad') {
            postad(
                $purifier->purify($_REQUEST['id'])
                , $purifier->purify($_REQUEST['user'])
                , $purifier->purify($_REQUEST['titel'])
                , $purifier->purify($_REQUEST['message'])
                , $purifier->purify($_REQUEST['price'])
                , $purifier->purify($_REQUEST['mail'])
                , $purifier->purify($_REQUEST['phone'])
                , $db);
            return true;
        }
        if($_REQUEST['mode']=='account') {
            $pass=$purifier->purify($_REQUEST['register_password']);
            $user=$purifier->purify($_REQUEST['user']);
            $adsPage=$purifier->purify($_REQUEST['adsPage']);

            ob_start();
            if(verify("", $pass, "I'm here only for verifying password","") || $pass=="")  //ether PW is valid(for change) or empty, if user doesn't want to change his PW.
            {
                ob_end_clean();
                return updateAccount(
                    $pass
                    , $user
                    , $adsPage
                    , $db);
            }
            ob_end_clean();
        }
        if($_REQUEST['mode']=='addelete') {

            return deleteAd(
                $purifier->purify($_REQUEST['id']),
                $purifier->purify($_REQUEST['user']),
                $db);

        }
        echo '<div id="main">Error.modeOrPassword</div>';
        return false;

    }
    else{
        echo '<div id="main">Error.LoginOrCommunication</div>';
        return false;
    }


}catch (PDOException $e){
    $error = $e->getMessage();
}
if (isset($error)){
    echo $error;
}

function getUserRole($uname,$db)
{
    $stmt = $db->prepare("Select * from User WHERE user = :username");
    $stmt->bindParam(':username', $uname);

    if($stmt->execute()) {
        if ($stmt->rowCount() > 0 )
        {
            $row = $stmt->fetch();
            return $row['role'];
        }
    }
}
/**
 * @param $uname
 * @param $upass
 * @return bool////
 */
function deleteAd($id,$uname,$db)
{

    if($_SESSION['user_session']==$uname or getUserRole($_SESSION['user_session'],$db)==1)
    {
        $stmt = $db->prepare("Select * from User WHERE user = :username");
        $stmt->bindParam(':username', $_SESSION['user_session']);
        if($stmt->execute()) {
            $row = $stmt->fetch();
            $user_ID = $row['ID'];
//                Get primary key of User in Session.

            if ($stmt->rowCount() > 0 )
            {
                $timestamp = date('Y-m-d H:i:s',time());

                $status= 'D'; // for deleting
                $stmt = $db->prepare("Update ads set status=:status where ID=:ID and User=:user");
                $stmt->bindParam(':ID',$id); //Advertise ID
                $stmt->bindParam(':status',$status);
                $stmt->bindParam(':user',$user_ID);

                if($stmt->execute()) {
                    if ($stmt->rowCount() > 0) {
                        echo '<div id="main">'."Success.".$db->lastInsertId().'.'.$timestamp.'</div>';
                        return true;
                    }
                }
            }
        }
    }

}
function updateAccount($pass,$uname,$adsPage,$db)//$umail
{
        if( $_SESSION['user_session']==$uname or getUserRole($_SESSION['user_session'],$db)==1)// Ist User auch der, den er vorgibt zu sein laut Session?
        {
            $stmt = $db->prepare("Select * from user WHERE user = :username");
            $stmt->bindParam(':username', $_SESSION['user_session']);
            if($stmt->execute()) {
//                Get primary key of User in Session.
                $row = $stmt->fetch();
                $user_ID = $row['ID'];

                if ($stmt->rowCount() > 0 ) // Ist User auch der, den er vorgibt zu sein laut Session?
                {

                    $timestamp = date('Y-m-d H:i:s',time());

                    if(strlen($pass)>0)
                    {
                        ob_start();
                        if(verify("", $pass, "I'm here only for verifying password",""))
                        {
                            ob_end_clean();
                            $salt=bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_RANDOM));
                            $saltedPW=mysqli_real_escape_string($pass).$salt;

                            $hashedPW= hash('sha256',$saltedPW);
                            $stmt = $db->prepare("Update user set adsPage=:adsPage, pass=:pass  where ID=:ID");
                            $stmt->bindParam(':ID',$user_ID);
                            $stmt->bindParam(':adsPage',$adsPage);
                            $stmt->bindParam(':pass',$hashedPW);
                            if($stmt->execute()) {
                                if ($stmt->rowCount() > 0) {
                                    $_SESSION['elementsPerpage'] = $adsPage;
                                    echo '<div id="main">' . "Success." . $db->lastInsertId() . '.' . $timestamp . '</div>';
                                    return true;
                                }
                            }
                        }
                        return false;

                    }
                    $stmt = $db->prepare("Update user set adsPage=:adsPage where ID=:ID");
                    $stmt->bindParam(':ID',$user_ID);
                    $stmt->bindParam(':adsPage',$adsPage);

                    if($stmt->execute()) {
                        if ($stmt->rowCount() > 0) {
                            $_SESSION['elementsPerpage'] = $adsPage;
                            echo '<div id="main">'."Success.".$db->lastInsertId().'.'.$timestamp.'</div>';
                            return true;
                        }
                    }
                }
            }
        }


}
//
function postad($id,$uname,$titel,$message,$price,$email,$phone,$db)//$umail
{
    $stmt = $db->prepare("Select * from User WHERE user = :username");
    $stmt->bindParam(':username', $_SESSION['user_session']);
//        $stmt->bindParam(':password', $hashedPW);

    if($stmt->execute()){
        //Get primary key of User in Session.
        $row = $stmt->fetch();
        $user_ID = $row['ID'];

        if($stmt->rowCount()>0)
        {
            $stmt = $db->prepare("Select User from ads where ID=:ID");
            $stmt->bindParam(':ID', $id);
            if($stmt->execute()){
                $row = $stmt->fetch();
                $user_ID_Ads = $row['User'];

                $timestamp = date('Y-m-d H:i:s',time());

                if( $user_ID_Ads==$user_ID or getUserRole($_SESSION['user_session'],$db)==1)  //Very important: Is the data which is gonna be updated really the session owner one?oder Admin?(User Role)
                {
                    $stmt = $db->prepare("Update ads set name=:adname, titel=:titel, message=:message, timestamp=:adtimestamp, price=:price, email=:email, phone=:phone where ID=:ID");
                    //                $status = "A";
                    $stmt->bindParam(':ID', $id);
                    $stmt->bindParam(':adname', $uname); //$uname
                    $stmt->bindParam(':titel', $titel);
                    $stmt->bindParam(':message', $message);
                    $stmt->bindParam(':price', $price); //$uname
                    $stmt->bindParam(':email', $email);
                    $stmt->bindParam(':phone', $phone);


                    $stmt->bindParam(':adtimestamp', $timestamp);

//                        $stmt->bindParam(':user', $user_ID);
                    if($stmt->execute()){
                        //                    echo  "success";
                        //                    echo $db->lastInsertId();
                        if($stmt->rowCount()>0)
                        {
                            //ID zurückbekommen vom zuletzt abgesetzten Datensatz.

                            echo '<div id="main">'."Success.".$db->lastInsertId().'.'.$timestamp.'</div>';
                            echo '<div id="main_name">'."irgendwer".'</div>'; //"irgendwer" = $uname
                            echo '<div id="main_titel">'.$titel.'</div>';
                            echo '<div id="main_message">'.$message.'</div>';
                            return true;
                        }
                    }

                }
                else{
                    echo '<div id="main">'."Error.updating.".$timestamp.'</div>';
                }


            }
        }
        return false;
    }

}
