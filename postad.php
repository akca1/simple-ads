<?php
session_start();
require_once 'htmlpurifier/library/HTMLPurifier.auto.php';
include_once ('config.inc.php');


try {
    $db = new PDO(mysql,dbuser,dbpass);


    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $clean_html = $purifier->purify($_REQUEST['message']);

    if(isset($_SESSION['user_session']))
    {
        postad($purifier->purify($_REQUEST['name'])
            ,$purifier->purify($_REQUEST['titel'])
            ,$purifier->purify($_REQUEST['message'])
            ,$purifier->purify($_REQUEST['phone'])
            ,$purifier->purify($_REQUEST['email'])
            ,$purifier->purify($_REQUEST['price'])
            ,$db);
    }
    else{
        echo '<div id="main">Error.Login</div>';
        return true;
    }


}catch (PDOException $e){
    $error = $e->getMessage();
}
if (isset($error)){
    echo $error;
}

/**
 * @param $uname
 * @param $upass
 * @return bool////
 */

function postad($uname,$titel,$message,$phone,$email,$price,$db)//$umail
{
    try
    {

        $stmt = $db->prepare("Select * from user WHERE user = :username");
        $stmt->bindParam(':username', $_SESSION['user_session']);
//        $stmt->bindParam(':password', $hashedPW);

        if($stmt->execute()){
            //Get primary key of last inserted User.
            $row = $stmt->fetch();
            $user_ID = $row['ID'];

            if($stmt->rowCount()>0)
            {
                $stmt = $db->prepare("Insert into ads(name,titel,message,timestamp,status,User,phone,email,price) VALUES(:name,:titel,:message,:timestamp,:status,:user,:phone,:email,:price)");
                $status = "A";
                $timestamp = date('Y-m-d H:i:s',time());
                $stmt->bindParam(':name', $uname);
                $stmt->bindParam(':titel', $titel);
                $stmt->bindParam(':message', $message);
                $stmt->bindParam(':timestamp', $timestamp);
                $stmt->bindParam(':user', $user_ID);
                $stmt->bindParam(':phone', $phone);
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':price', $price);

                $stmt->bindParam(':status', $status);


//                $str = $stmt->errorinfo();
//                print_r( $str[1]);

                if($stmt->execute()){

                    if($stmt->rowCount()>0)
                    {
                        $timestamp = date('d.m.y G:i',time());
                        //ID zurückbekommen vom zuletzt abgesetzten Datensatz.

                        echo '<div id="main">'."Success@".$timestamp.'</div>';
                        echo '<div id="main_name">'.$uname.'</div>';
                        echo '<div id="main_titel">'.$titel.'</div>';
                        echo '<div id="main_message">'.$message.'</div>';
                        echo '<div id="main_phone">'.$phone.'</div>';
                        echo '<div id="main_email">'.$email.'</div>';
                        echo '<div id="main_price">'.$price.'</div>';
                        return true;
                    }
                }
            }

            return false;
        }
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
