<?php
session_start();
require_once 'htmlpurifier/library/HTMLPurifier.auto.php';
include_once ('config.inc.php');

try {
    $db = new PDO(mysql,dbuser,dbpass);
    login($_REQUEST['login_username'],$_REQUEST['login_password'],$db);

}catch (PDOException $e){
    $error = $e->getMessage();
}
if (isset($error)){
    echo $error;
}

/**
 * @param $uname
 * @param $upass
 * @return bool////
 */


function login($uname, $upass,$db)//$umail
{
    try
    {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $upass = $purifier->purify($upass);

        $stmt = $db->prepare("Select * from User WHERE user = :username");
        $stmt->bindParam(':username', $uname);
//        $stmt->bindParam(':password', $hashedPW);

        if($stmt->execute()){

            //Get primary key of last inserted User.

            if($stmt->rowCount()>0)
            {
                $row = $stmt->fetch();
                $userID  = $row[ID];
                $hashedPW= $row[pass];
                $elementsPerpage = $row[adsPage];

                $stmt = $db->prepare("Select salt from SALT WHERE user = :user");
                $stmt->bindParam(':user',$userID);
                if($stmt->execute()) {
                    if ($stmt->rowCount() > 0) {
                        $row = $stmt->fetch();

                        $salt=$row[salt];

                        $saltedPW=$upass.$salt;

                        if(hash('sha256',$saltedPW)==$hashedPW){

                            $_SESSION['user_session'] = $uname;
                            $_SESSION['elementsPerpage'] = $elementsPerpage;
                            echo '<div id="main">'."Success:".$_SESSION['user_session'].'</div>';
                            return true;

                        } else{
                            echo '<div id="main">'."ERROR:".$row[user].'</div>';
                            return false;
                        }


                    }

                }

            }

            return false;
        }
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
