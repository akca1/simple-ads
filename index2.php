<?php
session_start();
include_once ('config.inc.php');

$mysql = 'mysql:host=127.0.0.1;port=4444;dbname=testuser';
/**
 * Created by PhpStorm.
 * User: Mehmet
 * Date: 16.11.16
 * Time: 18:25
 */
echo
require_once('Mustache/Autoloader.php');
Mustache_Autoloader::register();
$mustache = new Mustache_Engine(array(
    'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/templates')
));

$template = $mustache->loadTemplate('index');



if(isset($_SESSION['user_session'])){
    $user_session = $_SESSION['user_session'];
    $dropdown_status = '';
}
else{
    $user_session = '';
    $dropdown_status = 'disabled';
}


if(!isset($_SESSION['elementsPerpage']))
{
    $elementsPerpage = 5;
}
else{$elementsPerpage = $_SESSION['elementsPerpage'];};


if(isset($_GET["page"])){
    $page = $_GET["page"];}
else{$page=1;};

if(isset($_GET["user"])){
    $user = $_GET["user"];}
else{$user=null;};
//
//$_SESSION['elementsPerpage'] = 3;


//echo $_GET["page"];
//echo $_GET["elementsPerpage"];

$db = new PDO($mysql,'user','pass');

$element = DynContent($page,$user,$db);

//var_dump($element);
$numberOfAds = AdsCount($user,$db);

echo $template->render(

    array_merge(
        array('username' => $user_session,
            'dropdown_status' => $dropdown_status,
            'elementsPerpage' => $elementsPerpage,
            'numberads' => $numberOfAds,
            'pwvalidateregex' => email_valid_regex )
        ,
        $element
    )
);

function AdsCount($user,$db){

    $stmt = $db->prepare("Select * from User WHERE user = :username");
    $stmt->bindParam(':username', $user);

    if($stmt->execute()) {
        //Get primary key of last inserted User.
        $row = $stmt->fetch();
        $user_ID = $row['ID'];
        if(!is_null($user_ID)){  //Load all Content if User doesn't exist in DB, or wasn't hand over by hover was asking for this content.
            $stmt = $db->prepare("Select * from ads WHERE status = 'A' and User = :user order by timestamp desc");
            $stmt->bindParam(':user',$user_ID);
        }
        else{
            $stmt = $db->prepare("Select * from ads WHERE status = 'A' order by timestamp desc");

        }
        if ($stmt->execute()) {
            $row = $stmt->fetchAll();
            return $stmt->rowCount();
        }
    }
}

function DynContent($page,$user,$db){

    $stmt = $db->prepare("Select * from User WHERE user = :username");
    $stmt->bindParam(':username', $user);

    if($stmt->execute()) {
        //Get primary key of last inserted User.
        $row = $stmt->fetch();
        $user_ID = $row['ID'];
        $user_role=$row['role'];
        if(!is_null($user_ID)and $user_role==0){  //Load all Content if User doesn't exist in DB, or wasn't hand over by hover was asking for this content.
            $stmt = $db->prepare("Select * from ads WHERE status = 'A' and User = :user order by timestamp desc");
            $stmt->bindParam(':user',$user_ID);
        }
        else{
            $stmt = $db->prepare("SELECT * FROM User INNER JOIN ads ON ads.User=User.ID where status='A' order by timestamp DESC");

        }
        if(!isset($_SESSION['elementsPerpage']))
        {
            $elementsPerpage = 5;
        }
        else{$elementsPerpage = $_SESSION['elementsPerpage'];};

        if ($stmt->execute()) {
            $row = $stmt->fetchAll();

            if(!is_null($user_ID)){
                $Content_titel = "Welcome ".$user."!";
                if($user_role==1){
                    $Content = "Nice to see you today, these are all active ads by all User!";
                }
                else {
                    $Content = "Nice to see you today, these are your current ads. Good Luck!";
                }
            }else{
                $Content_titel = "Craiglist ";
                $Content = "Total number of classified advertisements: ".$stmt->rowCount();

            }

            $count = 0;
            $order = 0;
            foreach ($row as $post) {
                $username="";
                if($post['price']<>null){$post['price']=$post['price']."€";}
                if($user_role==1){$username=$post['user'];} //only Admin roles can see usernames of ads
                if ($count >= $elementsPerpage * $page - $elementsPerpage) { //
                    $element['ads'][$order] = array(
                        'titel' => $post['titel'],
                        //    'subject' => htmlspecialchars($post['message'], ENT_QUOTES,'ISO-8859-1', true),
                        'subject' => $post['message'],
                        'timestamp' => date("d.m.y G:i",strtotime($post['timestamp'])), //"F j, Y, g:i a",
                        'AdKey' => $post['ID'],
                        'price' => $post['price'],
                        'name' => $post['name'],
                        'email' => $post['email'],
                        'phone' => $post['phone'],
                        'username'=> $username
                    );
                    $order++;
                    if ($count == $elementsPerpage * $page -1) {
                        break;
                    }
                    //echo  htmlspecialchars($post['message'], ENT_QUOTES,'ISO-8859-1', true);
                }
                $count++;
            }
            return
                Array_merge(
                    array('Content_titel' => $Content_titel),
                    array('Content' => $Content),
                    $element
                );
        }
    }
}
?>
