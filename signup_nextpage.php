<?php
session_start();
require_once 'htmlpurifier/library/HTMLPurifier.auto.php';
include_once ('config.inc.php');
require_once "verify.php";

try {

   $db = new PDO(mysql,dbuser,dbpass);


    if(verify($_REQUEST['register_username'],$_REQUEST['register_password'],$_REQUEST['register_email'],$db))
    {
        insert($_REQUEST['register_username'],$_REQUEST['register_password'],$_REQUEST['register_email'],$db);

    };

}catch (PDOException $e){
    $error = $e->getMessage();
}
if (isset($error)){
    echo $error;
}

/**
 * @param $uname
 * @param $upass
 * @return bool////
 */


function insert($uname, $upass,$mail,$db)//$umail
{
    try
    {

        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $upass = $purifier->purify($upass);

        $salt=bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_RANDOM));
        $saltedPW=$upass.$salt;

        $hashedPW= hash('sha256',$saltedPW);


        $stmt = $db->prepare("Insert into user(user,pass,adsPage,role,email) VALUES (:username,:password,:adsPage,:role,:email)");
        $adsPage =5;
        $role=0;//0=user,1=admin
        $stmt->bindParam(':username', $uname);
        $stmt->bindParam(':password', $hashedPW);
        $stmt->bindParam(':adsPage', $adsPage);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':email', $mail);
        if($stmt->execute()){
            //Get primary key of last inserted User.
            $stmt = $db->prepare("Select ID from user where user = :username");
            $stmt->bindParam(':username', $uname);
            $stmt->execute();
            $row = $stmt->fetch();
            $user_ID = $row['ID'];
            //Insert Salt into sister table with primary key of the User Table (ID)
            $stmt = $db->prepare("Insert into salt(user,salt) VALUES (:userID,:salt)");
            $stmt->bindParam(':userID', $user_ID);
            $stmt->bindParam(':salt', $salt);
            if($stmt->execute()){
            }

// besser machen
            session_cache_limiter(20);
            $_SESSION['user_session'] = $uname;

            echo '<div id="main">'."Success:".$_SESSION['user_session'].'</div>';
            return true;
        }

    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}

function checkEmailAndDomain($_email)
{
    $exp = "/^(.*)@(.*)$/";
    preg_match($exp, $_email, $matches);

    if (!empty($matches[1]) and (!filter_var($_email, FILTER_VALIDATE_EMAIL)))
        return (false);

    return (checkdnsrr(idn_to_ascii($matches[2]), 'MX'));
}

